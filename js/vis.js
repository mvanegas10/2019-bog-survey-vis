(function() {
    // viewport height
    var viewportHeight = window.innerHeight
    var txtoffset = viewportHeight * (window.isMobile ? 0.75 : 0.5)

    function graphscroll1(data, maps) {
        // select elements using d3 here since this is a d3 library...
        var graphicEl = d3.select('.graphic')
        var graphicVisEl = d3.select('.graphic__vis')
        var triggerEls = d3.selectAll('.trigger')

        // a global function creates and handles all the vis + updates
        var graphic = createGraphic(data, maps)

        // this is it, graph-scroll handles pretty much everything
        // it will automatically add class names to the elements,
        // so you just need to handle the fixed positions with css
        d3.graphScroll()
            .eventId('uniqueId1')
            .container(graphicEl)
            .graph(graphicVisEl)
            .sections(triggerEls)
            .offset(txtoffset)
            .on('active', function(i) {
                graphic.update(i)
            })
    }

    function graphscroll2(partyData, personData, maps) {
        // select elements using d3 here since this is a d3 library...
        var graphicEl2 = d3.select('.graphic2')
        var graphicVisEl2 = d3.select('.graphic2__vis')
        var triggerEls2 = d3.selectAll('.trigger2')

        // a global function creates and handles all the vis + updates
        var graphic2 = createGraphic2(partyData, personData, maps)

        // this is it, graph-scroll handles pretty much everything
        // it will automatically add class names to the elements,
        // so you just need to handle the fixed positions with css
        d3.graphScroll()
            .eventId('uniqueId2')
            .container(graphicEl2)
            .graph(graphicVisEl2)
            .sections(triggerEls2)
            .offset(txtoffset)
            .on('active', function(i) {
                graphic2.update(i)
            })
    }

    d3.json('../data/ball_chart_data.json', function(errorA, data) {
        d3.json('infoMaps.json', function(errorB, maps) {
            d3.json('party_data.json', function(errorA, partyData) {
                d3.json('person_data.json', function(errorB, personData) {
                    graphscroll1(data, maps)
                    graphscroll2(partyData, personData, maps)
                })
            })
        })
    })
})()
